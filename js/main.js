$(function() {
  var navbar = $("#navbar").clone();
  var leftMenu = $(".left-menu");
  var doc = $(document);
  var fixedClass = "navbar-fixed animated slideInDown";
  navbar.addClass(fixedClass);
  $(".banner").append(navbar);

  $(".btnMenu").click(function() {
    leftMenu.animate({
      width: "toggle"
    }, 150);

    return false;
  });
  leftMenu.click(function() {
    $(this).animate({
      width: "toggle"
    }, 150);
  });
  doc.scroll(function() {
    if (doc.scrollTop() > 40) {
      navbar.fadeIn("slow");

    } else {
      navbar.fadeOut("slow");
    }
  });

  $('a[href^="#"]').on('click', function(event) {

    var target = $($(this).attr('href'));

    if (target.length) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top - 5
      }, 800);
    }
  });
});